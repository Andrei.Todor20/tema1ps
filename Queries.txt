CREATE DATABASE PARTIE;


USE PARTIE;


CREATE TABLE customer(
cutomer_id INT PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(15),
last_name VARCHAR(10),
email VARCHAR(30),
passw VARCHAR(15),
phone VARCHAR(10)
);


CREATE TABLE rent(
rent_id INT PRIMARY KEY AUTO_INCREMENT,
rent_time INT,
customer_id INT,
rent_total INT
);


CREATE TABLE rent_details(
rent_details_id INT PRIMARY KEY AUTO_INCREMENT,
product_id INT,
product_quantity INT,
product_price INT,
rent_id INT
);


CREATE TABLE product(
product_id INT PRIMARY KEY AUTO_INCREMENT,
product_name VARCHAR(20),
price INT,
stock INT,
category_id INT
);


CREATE TABLE category(
category_id INT PRIMARY KEY AUTO_INCREMENT,
category_name VARCHAR(15)
);

ALTER TABLE Rent ADD CONSTRAINT FOREIGN KEY(customer_id) REFERENCES Customer(customer_id);

ALTER TABLE Rent_Details ADD CONSTRAINT FOREIGN KEY(product_id) REFERENCES Product(product_id);

ALTER TABLE Rent_Details ADD CONSTRAINT FOREIGN KEY(rent_id) REFERENCES Rent(rent_id);

ALTER TABLE Product ADD CONSTRAINT FOREIGN KEY(category_id) REFERENCES Category(category_id);

DESCRIBE Rent_Details;